<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table='chat';
    protected $fillable=['sender', 'receiver', 'message'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }


}
