<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatImage extends Model
{
    protected $table='chat_images';
    protected $fillable=['chat_id', 'image'];

    public function chat()
    {
        return $this->belongsTo('App\Chat','chat_id');
    }
}
