<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreparationOrder extends Model
{
    protected $table='preparations_order';
    protected $fillable=['preparation_booking_id', 'services_sub_sub_category_id', 'quantity'];

    public function prepBooking()
    {
        return $this->belongsTo('App\PreparationBooking','preparation_booking_id');
    }

    public function serviceSubSubCategory()
    {
        return $this->belongsTo('App\ServiceSubSubCategory','services_sub_sub_category_id');
    }
}
